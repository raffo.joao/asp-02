import os
from flask import Flask, request, render_template
import requests

app = Flask(__name__)

@app.route('/')
def home():
    return render_template('form.html')

@app.route('/soma', methods=['POST'])
def soma():
    valor1 = int(request.form['valor1'])
    valor2 = int(request.form['valor2'])
    resultado = valor1 + valor2
    return f'O resultado da soma é: {resultado}'

if __name__ == '__main__':
    r = requests.get('http://localhost:4040/api/tunnels')
    ngrok_url = r.json()['tunnels'][0]['public_url']
    print("ngrok url " + ngrok_url)
    os.environ['NGROK_URL'] = ngrok_url
    app.run(debug=True, host='0.0.0.0')

FROM python:3.9-slim-buster

WORKDIR /app

COPY requirements.txt .

RUN apt-get update && apt-get install -y --no-install-recommends \
    && apt-get install -y bandit \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# RUN curl -s https://ngrok-agent.s3.amazonaws.com/ngrok.asc | sudo tee /etc/apt/trusted.gpg.d/ngrok.asc >/dev/null \
#     && echo "deb https://ngrok-agent.s3.amazonaws.com buster main" | sudo tee /etc/apt/sources.list.d/ngrok.list \
#     && sudo apt update && sudo apt install ngrok

RUN pip3 install --no-cache-dir -r requirements.txt

RUN pip3 install requests

COPY . .

EXPOSE 5000

# CMD ["python3", "app.py"]
CMD python3 app.py
